'use strict';

/**
 * @ngdoc directive
 * @name futureAlbumApp.directive:carousel
 * @description
 * # Basic Carousel, takes two variables, carousel-items,
 * # carousel-options.
 * # the items is required to work, options are not required.
 */
angular.module('futureAlbumApp')
  .directive('carousel', function ($interval, $document) {
    return {
      restrict: 'E',
      scope: {
        carouselItems: '=',
        carouselOptions: '='
      },
      templateUrl: 'templates/carousel.html',
      link: function(scope, element, attrs) {
        //Init the carousel at the zero item in the array
        scope.currentCarousel = 0;

        //Check if CarouselOptions is present, if not use defaults
        if (!scope.carouselOptions) {
          scope.carouselOptions = {
            interval: 5000
          }
        }

        // Carousel Interval
        var interval = $interval(function() {
          if (scope.currentCarousel < scope.carouselItems.length - 1) {
            scope.currentCarousel++;
          } else {
            scope.currentCarousel = 0;
          }
        }, scope.carouselOptions.interval);

        // Carousel Controls
        scope.moveCarouselToIndex = function(index) {
          scope.currentCarousel = index;
          $interval.cancel(interval)
        }

      }
    };
  });
