'use strict';

/**
 * @ngdoc function
 * @name futureAlbumApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the futureAlbumApp
 */
angular.module('futureAlbumApp')
  .controller('MainCtrl', function ($scope, $interval) {
    $scope.carouselItems = [
      {
        'header'        : 'New album from Future.',
        'body'          : 'Lorem ipsum dolor sit amet, consectetur and unscing elit, sed do eiusmod tempor incid.',
        'buttonText'    : 'Listen To Album',
        'buttonLocation': '#/',
        'image'         : 'carousel-1'
      },
      {
        'header'        : 'Watch exclusive videos.',
        'body'          : 'Ut enim ad minim veniam, quis nostrud exercita on ullamco laboris nisi ut aliquip ex ea.',
        'buttonText'    : 'Watch Videos',
        'buttonLocation': '#/',
        'image'         : 'carousel-2'
      },
      {
        'header'        : 'Experience DS2 live.',
        'body'          : 'Duis aute irure dolor in reprehenderit in volup tate velit esse cillum dolore eu fugiat nu.',
        'buttonText'    : 'Buy Tickets',
        'buttonLocation': '#/',
        'image'         : 'carousel-3'
      }
    ];
    $scope.carouselOptions = {
      interval: 5000
    }
  });
