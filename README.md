# future-album

This project is generated with [yo angular generator](https://github.com/yeoman/generator-angular)
version 0.15.1.

## Requirements

Requires NPM and Bower

## Installing

run `npm install` and then `bower install`

## Build & development

Run `grunt build` for building and `grunt serve` for preview.

## Running Prod Server

Run `node server.js`
